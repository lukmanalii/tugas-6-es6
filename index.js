//Soal 1
const luaspersegi = (s) => {
	return s * s;
}
const kelilingpersegi = (s) => {
	return s * 4;
}

let persegi1 = luaspersegi(3);
let persegi2 = kelilingpersegi(3);
console.log("Luasnya adalah " + persegi1 + " cm2");
console.log("Kelilingnya adalah " + persegi2 + " cm");

//Soal 2
const firstName = "William";
const lastName = "Imoh";
const namaLengkap = `${firstName} ${lastName}`;
console.log(namaLengkap);

//Soal 3
var data = {
	firstName: "Muhammad",
	lastName: "Iqbal Mubarok",
	address: "Jalan Ranamanyar",
	hobby: "playing football",
};

function biodata(diri){
	return diri.firstName + "," + " " + diri.lastName + "," + " " + diri.address + "," + " " + diri.hobby;
}

var studentBiodata = biodata(data);
console.log(studentBiodata);

//Soal 4
let west = ["Will", "Chris", "Sam", "Holly"]
let east = ["Gill", "Brian", "Noel", "Maggie"]

let combined = [...west, ...east];
console.log(combined);

//Soal 5
const planet = "earth";
const view = "glass";
 
console.log('Lorem ' + view + ' dolor sit amet, ' + 'consectetur adipiscing elit, ' + planet); 